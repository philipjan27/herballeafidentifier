package mariz.usm.app.com.herballeafidentifier.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Database.HerbalDatabase;
import mariz.usm.app.com.herballeafidentifier.Helpers.ImageAnalyser;
import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.Models.Uses;
import mariz.usm.app.com.herballeafidentifier.R;
import mariz.usm.app.com.herballeafidentifier.Views.CustomAutoCompleteTextview;
import mariz.usm.app.com.herballeafidentifier.Views.HerbalContentFragment;

public class HerbalSearchActivity extends AppCompatActivity  {

    public final static String TAG = HerbalSearchActivity.class.getSimpleName();


    List<HashMap<String, String>> hashMaps;
    List<Uses> usesList;
    SimpleAdapter adapter;
    CustomAutoCompleteTextview autoCompleteTextView;
    String[] from = {"illness", "herbalname"};
    int[] to = {R.id.autocomplete_herbalname, R.id.autocomplete_illness};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_herbal_search);

        autoCompleteTextView= findViewById(R.id.autocomplete_search);

        Typeface typeface= ResourcesCompat.getFont(this,R.font.caviar_dreams);
        autoCompleteTextView.setTypeface(typeface);

    }

    @Override
    protected void onResume() {
        super.onResume();
        hashMaps = new ArrayList<>();
        usesList = HerbalDatabase.getInstance(this).getUsesDao().getListOfHerbalUses();

        for (Uses s : usesList) {
            HashMap<String, String> map = new HashMap<>();
            map.put("illness", s.getIllness());
            map.put("herbalname", s.getHerbalName());
            map.put("herbalid", String.valueOf(s.getHerbal_id()));

            hashMaps.add(map);
        }

        initAdapter();
    }

    private void initAdapter(){
        adapter= new SimpleAdapter(this,hashMaps,R.layout.autocompletetextview_customlayout,from,to);
        autoCompleteTextView.setAdapter(adapter);

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                hideSoftKeyboard(HerbalSearchActivity.this);
                HashMap<String,String> map= (HashMap<String,String>) adapterView.getAdapter().getItem(i);
                Log.e(TAG, "onItemClick: HERBAL ID: "+map.get("herbalid"));
                displayHerbalDetails(Integer.parseInt(map.get("herbalid")));
            }
        });
    }

    private void displayHerbalDetails(int herbalId) {

        Bundle args= new Bundle();
        args.putInt("key",herbalId);
        HerbalContentFragment frag= new HerbalContentFragment();
        frag.setArguments(args);

        FragmentTransaction fm= getSupportFragmentManager().beginTransaction();
        fm.replace(R.id.herbalcontent_container,frag);
        fm.addToBackStack("content");
        fm.commit();

    }

    private void hideSoftKeyboard(Activity act) {
        InputMethodManager inputMethodManager= (InputMethodManager)act.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(),0);
    }


}
