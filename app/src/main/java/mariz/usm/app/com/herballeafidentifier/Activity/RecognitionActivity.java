package mariz.usm.app.com.herballeafidentifier.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import mariz.usm.app.com.herballeafidentifier.Helpers.Classifier;
import mariz.usm.app.com.herballeafidentifier.Helpers.ImageAnalyser;
import mariz.usm.app.com.herballeafidentifier.Helpers.TensorFlowImageClassifier;
import mariz.usm.app.com.herballeafidentifier.R;
import mariz.usm.app.com.herballeafidentifier.Views.CustomCameraPreviewer;
import mariz.usm.app.com.herballeafidentifier.Views.HerbalContentFragment;


public class RecognitionActivity extends AppCompatActivity implements View.OnClickListener {


    Classifier classifier;
    Executor executor = Executors.newSingleThreadExecutor();


    FrameLayout frameLayout, fLayoutcapture, fLayoutHome;
    CustomCameraPreviewer previewer;
    Camera camera;
    Camera.PictureCallback pictureCallbackJPG;
    private final String TAG = RecognitionActivity.class.getSimpleName();
    private static final String MODEL_FILE = "file:///android_asset/Stripped.pb";
    private static final String LABEL_FILE = "file:///android_asset/labels.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognition);
        setViews();
        initializePermissions();
//        showCamera();
        initializeTensorFlow();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        super.onResume();
//        initializePermissions();
//       camera.startPreview();
    }


    @Override
    protected void onStop() {
        Log.e(TAG, "onStop: ");
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy: ");
        super.onDestroy();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                classifier.close();
            }
        });
    }

    private void showCamera() {

        camera = Camera.open();
        previewer = new CustomCameraPreviewer(this, camera);
        frameLayout = findViewById(R.id.camerapreview);
        frameLayout.addView(previewer);

        Camera.Parameters params = camera.getParameters();

        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
            params.set("orientation", "portrait");
            camera.setDisplayOrientation(90);
            params.setRotation(90);
        }

    }

    private void captureImage() {

        pictureCallbackJPG = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes, Camera camera) {
                new CreateBitmapAsync().execute(bytes);
                Log.e(TAG, "onPictureTaken: " + bytes.toString());
            }
        };

        camera.takePicture(null, null, pictureCallbackJPG);

    }

    private void displayCaptureImageDialog(final Bitmap bmp) {

        final Bitmap bmpToCompare = makeBitmapToCompare(bmp);
        try {

            Button dismiss, analyze;
            ImageView imgview;
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.previewdialog_recognition);

            imgview = dialog.findViewById(R.id.imageviewdialog_dialogpreview);
            dismiss = dialog.findViewById(R.id.buttondialog_dismiss);
            analyze = dialog.findViewById(R.id.buttondialog_analyze);

            imgview.setImageBitmap(bmp);

            dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    camera.startPreview();
                }
            });

            analyze.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImageAnalyser analyser = new ImageAnalyser(classifier, RecognitionActivity.this, camera, getLayoutInflater());
                    analyser.execute(bmpToCompare);
                    analyser.setCallback(new ImageAnalyser.HerbalNameViewDetails() {
                        @Override
                        public void getName(String name) {
                            Log.e(TAG, "getName: NAME: " + name);

                            Bundle args = new Bundle();
                            args.putString("key", name);
                            HerbalContentFragment herbalContentFragment = new HerbalContentFragment();
                            herbalContentFragment.setArguments(args);

                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.herbaldetails_container, herbalContentFragment);
                            ft.addToBackStack("herbal_content");
                            ft.commit();

                        }
                    });
                    dialog.dismiss();
                }
            });

            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

        } catch (NullPointerException e) {
            Toast.makeText(this, "Empty bitmap image!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frame_capture:
                captureImage();
                break;
            case R.id.frame_home:
                startActivity(new Intent(RecognitionActivity.this, MainActivity.class));
                break;

        }
    }

    private void setViews() {
        fLayoutcapture = findViewById(R.id.frame_capture);
        fLayoutHome = findViewById(R.id.frame_home);

        fLayoutcapture.setOnClickListener(this);
        fLayoutHome.setOnClickListener(this);


    }

    /**
     * mobilenet_version1.0_224 12_17_17
     */
    private void initializeTensorFlow() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    classifier = TensorFlowImageClassifier.create(getAssets(),
                            MODEL_FILE
                            , LABEL_FILE
                            , 224
                            , 128
                            , 128
                            , "input" //Mul based on the script retrain.py
                            , "final_result"); //pool_3/_reshape based on the script retrain.py; regarding on what architecture to use
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "run: initializeTensorFlow EXCEPTION!" + e.getMessage());
                }
            }
        });
    }


    public class CreateBitmapAsync extends AsyncTask<byte[], Void, Bitmap> {

        ProgressDialog pdialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdialog = new ProgressDialog(RecognitionActivity.this);
            pdialog.setIndeterminate(true);
            pdialog.show();
            pdialog.setCanceledOnTouchOutside(false);

        }

        @Override
        protected Bitmap doInBackground(byte[]... bytes) {

            Bitmap a, finalBmp;

            Log.d(TAG, "doInBackground: BYTE: " + bytes.length);
            Matrix m = new Matrix();
            m.postRotate(90);
            a = BitmapFactory.decodeByteArray(bytes[0], 0, bytes[0].length);

            finalBmp = Bitmap.createBitmap(a, 0, 0, a.getWidth(), a.getHeight(), m, true);
            return finalBmp;
        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {
            pdialog.dismiss();
            displayCaptureImageDialog(bitmap);
            super.onPostExecute(bitmap);
        }
    }

    /**
     * mobilenet_version1.0_224 12_17_17
     *
     * @param toBeConverTed
     * @return
     */
    private Bitmap makeBitmapToCompare(Bitmap toBeConverTed) {
        Bitmap bmp;
        Matrix mx = new Matrix();

        float w = ((float) 224) / toBeConverTed.getWidth();
        float h = ((float) 224) / toBeConverTed.getHeight();

        mx.postScale(w, h);

        bmp = Bitmap.createScaledBitmap(toBeConverTed, 224, 224, false);

        return bmp;

    }

    private void initializePermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            showCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showCamera();
                } else {
                    Toast.makeText(this, "Permission to use the camera denied!", Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }
}
