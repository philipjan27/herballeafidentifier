package mariz.usm.app.com.herballeafidentifier.Helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Activity.MainActivity;
import mariz.usm.app.com.herballeafidentifier.Database.HerbalDatabase;
import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.R;

/**
 * Created by Philip on 12/10/2017.
 */

public class ImageAnalyser extends AsyncTask<Bitmap, Void, List<Classifier.Recognition>> {

    HerbalNameViewDetails callback;

    public interface HerbalNameViewDetails {
        void getName(String name);
    }

    Classifier classifier;
    Context ctx;
    Camera cam;
    Bitmap capturedBmp;
    LayoutInflater inflater;

    public ImageAnalyser(Classifier classifier, Context ctx, Camera cam, LayoutInflater inflater) {
        this.classifier = classifier;
        this.ctx = ctx;
        this.cam = cam;
        this.inflater = inflater;
    }

    private static final String TAG = ImageAnalyser.class.getSimpleName();

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<Classifier.Recognition> doInBackground(Bitmap... bitmaps) {
        if (capturedBmp == null) {
            capturedBmp = Bitmap.createBitmap(bitmaps[0]);
        }
        List<Classifier.Recognition> recognitions = new ArrayList<>();
        recognitions = classifier.recognizeImage(bitmaps[0]);

        for (Classifier.Recognition rec : recognitions) {
            Log.e(TAG, "doInBackground: \n" + "Confidence: " + rec.getConfidence() + "\n" + "Name: " + rec.getTitle());
        }

        return recognitions;
    }

    @Override
    protected void onPostExecute(List<Classifier.Recognition> recognitions) {
        Classifier.Recognition rec = recognitions.get(0);
        Log.e(TAG, "onPostExecute:\nConfidence: " + rec.getConfidence() + "\n" + "Title: " + rec.getTitle());

        if (rec.getConfidence() < 0.8) {
            Log.e(TAG, "onPostExecute: No Image matches found!");
            Toast.makeText(ctx, "No image matches found, please try again", Toast.LENGTH_SHORT).show();
            cam.startPreview();
        } else {

            Toast.makeText(ctx, rec.getConfidence() + " " + rec.getTitle(), Toast.LENGTH_SHORT).show();
            Herbals herbalName = HerbalDatabase.getInstance(ctx).getHerbalDao().getHerbalName(rec.getTitle());

            if (herbalName == null) {
                Log.e(TAG, "onPostExecute: EMPTY!");
            } else {
                Log.e(TAG, "onPostExecute: NOT EMPTY! " + herbalName.getHerbalName());
                displayMatchesDialog(herbalName.getHerbalName());
            }

        }
        cam.startPreview();
    }

    private void displayMatchesDialog(final String herbalname) {

        ImageView captured, reference;
        Button viewDetails, dismiss;

        final Dialog customDialog = new Dialog(ctx);
        customDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(inflater.inflate(R.layout.matchesdialog_customlayout, null));

        captured = customDialog.findViewById(R.id.capturedImage);
        reference = customDialog.findViewById(R.id.referencedImage);
        viewDetails = customDialog.findViewById(R.id.match_viewdetails);
        dismiss = customDialog.findViewById(R.id.match_dismiss);

        captured.setImageBitmap(capturedBmp);
        getHerbalImageToDisplay(herbalname, reference);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                cam.startPreview();
            }
        });

        viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.getName(herbalname);
                customDialog.dismiss();
            }
        });

        customDialog.setCanceledOnTouchOutside(false);
        customDialog.setCancelable(false);
        customDialog.show();
    }

    public void getHerbalImageToDisplay(String herbalName, ImageView imgvw) {
        switch (herbalName) {
            case "akapulko":
                imgvw.setImageResource(R.drawable.akapulko);
                break;
            case "alugbati":
                imgvw.setImageResource(R.drawable.alugbati);
                break;
            case "ampalaya":
                imgvw.setImageResource(R.drawable.ampalaya);
                break;
            case "bayabas":
                imgvw.setImageResource(R.drawable.bayabas);
                break;
            case "gingerleaves":
                imgvw.setImageResource(R.drawable.ginger_leaves);
                break;
            case "gotukola":
                imgvw.setImageResource(R.drawable.gotu_kola);
                break;
            case "guyabano":
                imgvw.setImageResource(R.drawable.guyabano);
                break;
            case "herbabuena":
                imgvw.setImageResource(R.drawable.herba_buena);
                break;
            case "lagundi":
                imgvw.setImageResource(R.drawable.lagundi);
                break;
            case "malunggay":
                imgvw.setImageResource(R.drawable.malunggay);
                break;
            case "mayana":
                imgvw.setImageResource(R.drawable.mayana);
                break;
            case "okra":
                imgvw.setImageResource(R.drawable.okra);
                break;
            case "oregano":
                imgvw.setImageResource(R.drawable.oregano);
                break;
            case "pansitpansitan":
                imgvw.setImageResource(R.drawable.pansit_pansitan);
                break;
            case "saluyot":
                imgvw.setImageResource(R.drawable.saluyot);
                break;
            case "sambong":
                imgvw.setImageResource(R.drawable.sambong);
                break;
            case "sililabuyo":
                imgvw.setImageResource(R.drawable.sili);
                break;
            case "tanglad":
                imgvw.setImageResource(R.drawable.tanglad);
                break;
            case "tawatawa":
                imgvw.setImageResource(R.drawable.tawa_tawa);
                break;
            case "tsaanggubat":
                imgvw.setImageResource(R.drawable.tsaang_gubat);
                break;
        }
    }

    public void setCallback(HerbalNameViewDetails callback) {
        this.callback=callback;
    }

}
