package mariz.usm.app.com.herballeafidentifier.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Relation;

import java.util.List;

/**
 * Created by Philip on 12/10/2017.
 */

@Entity
public class Herbals {

    @PrimaryKey(autoGenerate = true)
    public int id;
    public int herbalIndicator;
    public String herbalName;
    public String scientificName;
    public String botanyDescription;

    @Ignore
    public Herbals(int id, int herbalIndicator, String herbalName, String scientificName, String botanyDescription) {
        this.id = id;
        this.herbalIndicator = herbalIndicator;
        this.herbalName = herbalName;
        this.scientificName = scientificName;
        this.botanyDescription = botanyDescription;
    }

    public Herbals(int herbalIndicator, String herbalName, String scientificName, String botanyDescription) {
        this.herbalIndicator = herbalIndicator;
        this.herbalName = herbalName;
        this.scientificName = scientificName;
        this.botanyDescription = botanyDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHerbalIndicator() {
        return herbalIndicator;
    }

    public void setHerbalIndicator(int herbalIndicator) {
        this.herbalIndicator = herbalIndicator;
    }

    public String getHerbalName() {
        return herbalName;
    }

    public void setHerbalName(String herbalName) {
        this.herbalName = herbalName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getBotanyDescription() {
        return botanyDescription;
    }

    public void setBotanyDescription(String botanyDescription) {
        this.botanyDescription = botanyDescription;
    }
}
