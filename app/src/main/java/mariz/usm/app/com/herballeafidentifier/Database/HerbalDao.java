package mariz.usm.app.com.herballeafidentifier.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Models.Herbals;

/**
 * Created by Philip on 12/18/2017.
 */

@Dao
public interface HerbalDao {

    @Query("SELECT * FROM herbals")
    List<Herbals>checkIfCurrentHerbalAlreadyExist();

    @Query("SELECT * FROM herbals WHERE id = :id")
     Herbals getSingleHerbalContent(int id);

    @Query("SELECT * FROM herbals")
    List<Herbals> getAllList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertHerbal(Herbals ...herbals);

    @Query("SELECT * FROM herbals WHERE herbalName LIKE :result")
    Herbals getHerbalName(String result);
}
