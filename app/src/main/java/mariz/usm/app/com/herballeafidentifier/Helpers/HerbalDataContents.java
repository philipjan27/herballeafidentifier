package mariz.usm.app.com.herballeafidentifier.Helpers;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Database.HerbalDatabase;
import mariz.usm.app.com.herballeafidentifier.Models.HerbalUsesPreparation;
import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.Models.Uses;
import mariz.usm.app.com.herballeafidentifier.R;

/**
 * Created by Philip on 12/28/2017.
 */

public class HerbalDataContents {

    private static HerbalDataContents instance=null;
    public final static String TAG = HerbalDataContents.class.getSimpleName();

    static Context ctx;

    public HerbalDataContents(Context ctx) {
        this.ctx = ctx;
    }

     public static HerbalDataContents getInstance(Context ctx) {
      if (instance == null) {
          instance= new HerbalDataContents(ctx);
      }
      return instance;
    }

    public void initDbContent() {


        Herbals oregano = new Herbals(1, "oregano", "Origanum vulgare", ctx.getResources().getString(R.string.oregano));
        Herbals alugbati = new Herbals(2, "alugbati", "Basella alba", ctx.getResources().getString(R.string.alugbati));
        Herbals mayana = new Herbals(3, "mayana", "Plectranthus scutellarioides", ctx.getResources().getString(R.string.mayana));
        Herbals lagundi = new Herbals(4, "lagundi", "Vitex Negundo", ctx.getResources().getString(R.string.lagundi));
        Herbals pansitpansitan = new Herbals(5, "pansitpansitan", "Peperomia pellucida Linn", ctx.getResources().getString(R.string.pansit_pansitan));
        Herbals gingerLeaves = new Herbals(6, "gingerleaves", "Zingiber officinale", ctx.getResources().getString(R.string.ginger_leaves));
        Herbals tsaangGubat = new Herbals(7, "tsaanggubat", "Carmona retusa", ctx.getResources().getString(R.string.tsaang_gubat));
        Herbals akapulko = new Herbals(8, "akapulko", "Cassia alata", ctx.getResources().getString(R.string.akapulko));
        Herbals guyabano = new Herbals(9, "guyabano", "Annona muricata", ctx.getResources().getString(R.string.guyabano));
        Herbals sambong = new Herbals(10, "sambong", "Blumea balsamifera", ctx.getResources().getString(R.string.sambong));
        Herbals tanglad = new Herbals(11, "tanglad", "Andropogon fragrans", ctx.getResources().getString(R.string.tanglad));
        Herbals malunggay = new Herbals(12, "malunggay", "Moringa oleifera", ctx.getResources().getString(R.string.malunggay));
        Herbals saluyot = new Herbals(13, "saluyot", "Corchorus olitorius", ctx.getResources().getString(R.string.saluyot));
        Herbals tawaTawa = new Herbals(14, "tawatawa", "Euphorbia hirta", ctx.getResources().getString(R.string.tawa_tawa));
        Herbals bayabas = new Herbals(15, "bayabas", "Psidium guajava", ctx.getResources().getString(R.string.bayabas));
        Herbals ampalaya = new Herbals(16, "ampalaya", "Momordica charantia", ctx.getResources().getString(R.string.ampalaya));
        Herbals gotuKola = new Herbals(17, "gotukola", "Centella asiatica ", ctx.getResources().getString(R.string.gotu_kola));
        Herbals herbaBuena = new Herbals(18, "herbabuena", "Mentha spicata", ctx.getResources().getString(R.string.herba_buena));
        Herbals okra = new Herbals(19, "okra", "Abelmoschus esculentus", ctx.getResources().getString(R.string.okra));
        Herbals siliLabuyo = new Herbals(20, "sililabuyo", "Capsicum Frutescens", ctx.getResources().getString(R.string.siling_labuyo));


/**
 *      OREGANO USES
 */
        List<String> oreganoUses = new ArrayList<>();
        oreganoUses.add("cough");
        oreganoUses.add("asthma");
        oreganoUses.add("bronchitis");
        oreganoUses.add("heartburn");
        oreganoUses.add("bloating");
        oreganoUses.add("menstrual cramps");
        oreganoUses.add("arthritis");
        oreganoUses.add("uti");
        oreganoUses.add("headaches");
        oreganoUses.add("heart conditions");
        oreganoUses.add("dyspepsia");
        oreganoUses.add("insect bites");
        oreganoUses.add("wounds");
        oreganoUses.add("stings");
        oreganoUses.add("sore throat");

        Uses oreganoUsesObj = new Uses(1, oregano.getHerbalName(), oreganoUses);

/**
 *      OREGANO PREPARATIONS
 */
        List<String> oreganoPreparationsList = new ArrayList<>();
        oreganoPreparationsList.add("Boil one cup of fresh leaves in 3 cups of water for 10 to 15 minutes. Drink half a cup 3 times a day for common colds");
        oreganoPreparationsList.add("For a concentrate, juice the oregano leaves and take 1 tablespoon every hour to relieve chronic coughs, rheumatism, bronchitis, asthma, and dyspepsia");
        oreganoPreparationsList.add("For Insect bites, wounds and stings, apply the leaves as a poultice directly on the afflicted area");
        oreganoPreparationsList.add("For sore throat, boil 2 tablespoonfuls of dried oregano leaves in a pint of water, take 2 hours before or after meals");
        oreganoPreparationsList.add("To prevent degenerative arthritis & for general good health drink oregano decoction daily");

        HerbalUsesPreparation oreganoPreparation = new HerbalUsesPreparation("bushy herb with a strong unique flavor that’s especially pungent when the herb is fresh",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/oregano", 1, oreganoPreparationsList);


        /**
         * ALUGBATI USES
         */
        List<String> alugbatiUses = new ArrayList<>();
        alugbatiUses.add("bowel movements");

        Uses alugbatiUsesObj = new Uses(2, alugbati.getHerbalName(), alugbatiUses);

        /**
         * ALUGBATI PREPARATIONS
         */
        List<String> alugbatiPreparationsList = new ArrayList<>();
        alugbatiPreparationsList.add("It can be cooked in the same way and, like spinach, is a good source of a number of essential nutrients, including iron");

        HerbalUsesPreparation alugbatiPreparation = new HerbalUsesPreparation("alugbati is thicker, and its leaves are juicier. It has more intense flavor when mixed in food.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/alugbati", 2, alugbatiPreparationsList);


        /**
         * MAYANA USES
         */

        List<String> mayanaUses = new ArrayList<>();
        mayanaUses.add("cough");
        mayanaUses.add("headaches");
        mayanaUses.add("bruises");

        Uses mayanaUsesObj = new Uses(3, mayana.getHerbalName(), mayanaUses);

        /**
         * MAYANA PREPARATIONS
         */

        List<String> mayanaPreparationsList = new ArrayList<>();
        mayanaPreparationsList.add("Cough: Infusion - put clean, fresh leaves into a pitcher of water and use as drinking water everyday");
        mayanaPreparationsList.add("Headache: Pound the leaves and extract the juice. Wipe the juice to the area of pain. It provides cooling effect to the head and can relieve headache.");
        mayanaPreparationsList.add("Swelling and Bruises: Press the leaves slightly to reveal its juice and put it on top of swelling area.");
        mayanaPreparationsList.add("Anti-inflammatory: Infusion - use the same formula for cough. Take mayana infusion as substitute for your water on a daily basis.");
        mayanaPreparationsList.add("Anti-oxidant: mayana contains special phytochemical that helps in fighting against free radicals that invade our bodies. Drink mayana herbal plant as infusion similar to cough remedy and anti-inflammatory remedy.");

        HerbalUsesPreparation mayanaPreparation = new HerbalUsesPreparation("bitter taste", "android.resource://mariz.usm.app.com.herballeafidentifier/raw/mayana", 3, mayanaPreparationsList);


        /**
         * LAGUNDI USES
         */

        List<String> lagundiUses = new ArrayList<>();
        lagundiUses.add("asthma");
        lagundiUses.add("pharyngitis");
        lagundiUses.add("rheumatism");
        lagundiUses.add("dyspepsia");
        lagundiUses.add("boils");
        lagundiUses.add("diarrhea ");

        Uses lagundiUsesObj = new Uses(4, lagundi.getHerbalName(), lagundiUses);

        /**
         * LAGUNDI PREPARATIONS
         */

        List<String> lagundiPreparationsList = new ArrayList<>();
        lagundiPreparationsList.add("Boil half cup of chopped fresh or dried leaves in 2 cups of water for 10 to 15 minutes. Drink half cup three times a day.");
        lagundiPreparationsList.add("For skin diseases or disorders, apply the decoction of leaves and roots directly on skin. ");
        lagundiPreparationsList.add("The root is specially good for treating dyspepsia, worms, boils, colic and rheumatism.");

        HerbalUsesPreparation lagundiPreparation = new HerbalUsesPreparation("It is pungent, bitter, and astringent in taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/lagundi", 4, lagundiPreparationsList);

        /**
         * PANSIT PANSITAN USES
         */
        List<String> pansitanUses = new ArrayList<>();
        pansitanUses.add("eye inflammation");
        pansitanUses.add("sore throat");
        pansitanUses.add("diarrhea");
        pansitanUses.add("prostate problems");
        pansitanUses.add("high blood pressure");
        pansitanUses.add("arthritis");
        pansitanUses.add("gout");
        pansitanUses.add("skin boils");
        pansitanUses.add("wounds");
        pansitanUses.add("burns");
        pansitanUses.add("skin inflammation");
        pansitanUses.add("abscesses");
        pansitanUses.add("pimples");
        pansitanUses.add("headache");
        pansitanUses.add("fever");
        pansitanUses.add("abdominal pains");
        pansitanUses.add("renal problems");
        pansitanUses.add("mental excitement disorder");

        Uses pansitanUsesObj = new Uses(5, pansitpansitan.getHerbalName(), pansitanUses);

        /**
         * PANSIT PANSITAN PREPARATIONS
         */
        List<String> pansitanPreparationsList = new ArrayList<>();
        pansitanPreparationsList.add("For the herbal treatment of disorders like abscesses, pimples and boils, pound the leaves and/or the stalks and make a poultice (boil in water for a minute or two then pounded) then applied directly to the afflicted area. Likewise a decoction can be used as a rinse to treat skin disorders.");
        pansitanPreparationsList.add("For headaches, heat a couple of leaves in hot water, bruise the surface and apply on the forehead. The decoction of leaves and stalks is also good for abdominal pains and kidney problems. ");

        HerbalUsesPreparation pansitanPreparation = new HerbalUsesPreparation("The leaves taste good but the stalks have a slight astringent taste like raw pechay stalks.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/pansit_pansitan", 5, pansitanPreparationsList);

        /**
         * GINGER LEAVES USES
         */
        List<String> gingerUses = new ArrayList<>();
        gingerUses.add("headache");
        gingerUses.add("migrane");
        gingerUses.add("fever");
        gingerUses.add("colon cancer");

        Uses gingerUsesObj = new Uses(6, gingerLeaves.getHerbalName(), gingerUses);

        /**
         * GINGER PREPARATIONS
         */
        List<String> gingerPreparationsList = new ArrayList<>();
        gingerPreparationsList.add("Prepare by chopping luya or ginger (200 grms - dried or 300 grams - fresh) for every liter of desired preparation." +
                "You may use as preparation vinegar, glycerol, distilled water or rum for alcohol based tincture (never use isopropyl, rubbing or any industrial grade alcohol)" +
                "Mix the luya or ginger with your preparation in a sterilized glass jar and seal it properly." +
                "Keep the jar in a dark area for about 2 weeks, shaking the glass jar every day." +
                "Filter with cheesecloth the luya or ginger liquid and transfer the tincture in a colored glass container.");

        HerbalUsesPreparation gingerPreparation = new HerbalUsesPreparation("taste in our mouths is not a taste that is actually a spicy scent.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/ginger", 6, gingerPreparationsList);

        /**
         * TSAANG GUBAT USES
         */

        List<String> tsaangGubatUses = new ArrayList<>();
        tsaangGubatUses.add("skin disease");
        tsaangGubatUses.add("stomach problems");

        Uses tsaangGubatUsesObj = new Uses(7, tsaangGubat.getHerbalName(), tsaangGubatUses);

        /**
         * TSAANG GUBAT PREPARATIONS
         */

        List<String> tsaangGubatPreparationsList = new ArrayList<>();
        tsaangGubatPreparationsList.add("Tsaang gubat Tea Preparation: " +
                "Pound or cut about half cup of Tsaang gubat leaves," +
                "Then add in 2 cups of water for" +
                "Boil for 10 to 15 minutes, with open pot cover," +
                "Let it steep and strain." +
                "Store in a glass jar." +
                "Consume within the day");
        tsaangGubatPreparationsList.add("Herbal tea. Take one cup three times a day.");
        tsaangGubatPreparationsList.add("Herbal wash. To be applied directly as a wash over the affected area.");
        tsaangGubatPreparationsList.add("Herbal gargle. Can be gargled to treat canker sores and for theething problems.");

        HerbalUsesPreparation tsaangGubatPreparation = new HerbalUsesPreparation("It is pungent, bitter, and astringent in taste.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/tsaang_gubat", 7, tsaangGubatPreparationsList);

        /**
         * AKAPULKO USES
         */
        List<String> akapulkoUses = new ArrayList<>();
        akapulkoUses.add("skin diseases");
        akapulkoUses.add("arthritis");
        akapulkoUses.add("gout");
        akapulkoUses.add("coughs");
        akapulkoUses.add("bursitis");
        akapulkoUses.add("rheumatitis");

        Uses akapulkoUsesObj = new Uses(8, akapulko.getHerbalName(), akapulkoUses);

        /**
         * AKAPULKO PREPARATIONS
         */

        List<String> akapulkoPreparationsList = new ArrayList<>();
        akapulkoPreparationsList.add("For Cold Process: " + "Macerate/soak the leaves in ethyl alcohol in the glass jar for at least 3 days. Cover and set aside. Add more alcohol to keep the leaves always immersed in the alcohol."
                + "On the 4th day, filter the extract through a clean piece of cheesecloth/filter paper." + "Using a water bath (in a big kettle with water, place your enamel \"tabo\" containing the extract) and under medium heat evaporate the solvent (the ethyl alcohol) until you get a thick, concentrated extract."
                + ". Upon reaching the desired consistency, remove the extract from the water bath." + "Mix thick extract with the white petrolatum in a 15% proportion (15 grams/1 Tablespoon extract for every 100 gms of white petrolatum) until the extract is blended well with the petrolatum."
                + "Transfer the akapulko ointment in the desired containers. Label properly.s");

        akapulkoPreparationsList.add("For Hot Process: " + "Fry the chopped leaves until crisp." + "Strain. Add the grated candle to the oil." + "Heat over low heat until all the candle wax is melted. Mix well."
                + "Transfer to ointment jars before the mixture hardens." + "Label properly.");

        HerbalUsesPreparation akapulkoPreparation = new HerbalUsesPreparation("bitter taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/akapulko", 8, akapulkoPreparationsList);

        /**
         * GUYABANO USES
         */

        List<String> guyabanoUses = new ArrayList<>();
        guyabanoUses.add("cancer");
        guyabanoUses.add("back pain");
        guyabanoUses.add("rheumatism");
        guyabanoUses.add("migrane");
        guyabanoUses.add("uti");
        guyabanoUses.add("constipation");
        guyabanoUses.add("anemia");
        guyabanoUses.add("leg cramps");
        guyabanoUses.add("herpes");
        guyabanoUses.add("hypertension");
        guyabanoUses.add("diabetes");

        Uses guyabanoUsesObj = new Uses(9, guyabano.getHerbalName(), guyabanoUses);

        /**
         * GUYABANO PREPARATIONS
         */

        List<String> guyabanoPreparationsList = new ArrayList<>();
        guyabanoPreparationsList.add("Add a few dried guyabano leaves to a pot with boiling water." + "Put the flame in medium and cover the pot and let the leaves to simmer for about five to ten minutes." +
                "You can leave them in there for up to thirty minutes as most nutrients will be drawn." + "You can have the tea about three times in an eight hour period." +
                "To have better taste you can add a couple teaspoons sugar –whether it’s brown or white sugar– into your tea. Or for optimum health effect you can add some wild honey to sweeten your tea. While your tea giving you dozens of benefits, the honey can make additional ones." +
                " It have the ability to cure your sore throats, improve your immune system, and keep blood pressure under-control.");

        HerbalUsesPreparation guyabanoPreparation = new HerbalUsesPreparation("Processed or ground tea usually has some ingredients added to it to make have a sweeter taste and appeal to a large scale audience.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/guyabano", 9, guyabanoPreparationsList);


        /**
         *SAMBONG USES
         */

        List<String> sambongUses = new ArrayList<>();
        sambongUses.add("hypertension");
        sambongUses.add("renal failure");
        sambongUses.add("kidney stones");
        sambongUses.add("cancer");
        sambongUses.add("antifungi");

        Uses sambongUsesObj = new Uses(10, sambong.getHerbalName(), sambongUses);

        /**
         * SAMBONG HERBALPREPARATIONS
         */

        List<String> sambongPreparationList = new ArrayList<>();
        sambongPreparationList.add("sambong tea preparation: " +
                "gather fresh sambong leaves, cut in small pieces " +
                "wash with fresh water. " +
                "Boil 50 grams of sambong leaves to a liter of water. " +
                "Let it seep for 10 minutes." +
                "Remove from heat. " +
                "Drink while warm 4 glasses a day for best results. ");

        sambongPreparationList.add("sambong poultice: " +
                "Gather fresh leaves and roots. " +
                "Wash with fresh clean water. " +
                "Pound in a mortar. " +
                "Grounded leaves may be applied or a juice extract may be used");

        HerbalUsesPreparation sambongPreparation = new HerbalUsesPreparation("Adding sugar and lemon would give you a better and desirable taste, but you still can perceive the bitter after taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/sambong", 10, sambongPreparationList);

        /**
         *TANGLAD USES
         */
        List<String> tangladUses = new ArrayList<>();
        tangladUses.add("stomachache");
        tangladUses.add("diarrhea");
        tangladUses.add("spasms");
        tangladUses.add("vomiting");
        tangladUses.add("fever");
        tangladUses.add("flu");
        tangladUses.add("headaches");

        Uses tangladUsesObj = new Uses(11, tanglad.getHerbalName(), tangladUses);

        /**
         * TANGLAD PREPARATIONS
         */
        List<String> tangladPreparationList = new ArrayList<>();
        tangladPreparationList.add("Tanglad as herbal Tea: (Taken 1 cup every 8 hours)");
        tangladPreparationList.add("Tanglad oil mixed with other essential oils such as lavender or jasmine oil used in baths or vapor scents, can revitalize the body and relieve the symptoms of jet lag, headaches, anxiety and stress related exhaustion.");
        tangladPreparationList.add("Tanglad is used in Indian Ayurvedic medicine to treat fevers and infectious illnesses.");
        tangladPreparationList.add("Tanglad is also used in Chinese medicine to treat colds and rheumatism");
        tangladPreparationList.add("To treat circulatory disorders, some authorities recommend rubbing a few drops of tanglad oil on the skin of affected areas; it is believed to work by improving blood flow.");
        tangladPreparationList.add("Tanglad has natural anti-microbial properties, is an antiseptic, suitable for use on various types of skin infections, usually as a wash or compress, and is especially effective on ringworm, infected sores. Acne and athlete's foot");
        tangladPreparationList.add("Tanglad is effective in killing cancer cells.");
        tangladPreparationList.add("It is useful with respiratory infections such as sore throats, laryngitis and fever and helps prevent spreading of infectious diseases.");
        tangladPreparationList.add("Tanglad is also used as an insect repellant. It helps to keep pets clean of fleas, ticks and lice.");
        tangladPreparationList.add("In some African countries, Tanglad is used to treat diabetes");

        HerbalUsesPreparation tangladPreparation = new HerbalUsesPreparation("milder and sweeter in taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/tanglad", 11, tangladPreparationList);

        /**
         * MALUNGGAY USES
         */
        List<String> malunggayUses = new ArrayList<>();
        malunggayUses.add("anemia");
        malunggayUses.add("rheumatism");
        malunggayUses.add("asthma");
        malunggayUses.add("epilepsy");
        malunggayUses.add("stomach pain");
        malunggayUses.add("ulcer");
        malunggayUses.add("kidney stones");
        malunggayUses.add("high blodd pressure");
        malunggayUses.add("thyroid disorders");

        Uses malunggayUsesObj = new Uses(12, malunggay.getHerbalName(), malunggayUses);

        /**
         * MALUNGGAY PREPARATIONS
         */
        List<String> malunggayPreparationList = new ArrayList<>();
        malunggayPreparationList.add(", it is best not to boil the leaves so that you can get the full nutritional benefits of malunggay tea. What you need to do is steep two teaspoonful of malunggay tea in boiling water. Allow the leaves to settle at the bottom of the cup before drinking the brew. You can add honey or sugar to the brew if you want a sweet-tasting tea.");

        HerbalUsesPreparation malunggayPreparation = new HerbalUsesPreparation("malunggay pods may be eaten raw or may also be fried with peanut similar taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/malunggay", 12, malunggayPreparationList);


        /**
         * SALUYOT USES
         */

        List<String> saluyotUses = new ArrayList<>();
        saluyotUses.add("eye problems");
        saluyotUses.add("wrinkles");
        saluyotUses.add("stomachache");
        saluyotUses.add("arthritis");
        saluyotUses.add("headache");
        saluyotUses.add("high blood pressure");
        saluyotUses.add("dysentery");

        Uses saluyotUsesObj = new Uses(13, saluyot.getHerbalName(), saluyotUses);

        /**
         * SALUYOT PREPARATIONS
         */
        List<String> saluyotPreparationList = new ArrayList<>();
        saluyotPreparationList.add("Boil half a cup of dried saluyot leaves to a liter of water. Let it seep for 10 minutes. Remove from heat. Strain solids. Drink while warm 3 - 4 glasses a day for best results");

        HerbalUsesPreparation saluyotPreparation = new HerbalUsesPreparation("are slimy and a bit bitter.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/saluyot", 13, saluyotPreparationList);


        /**
         * TAWA TAWA USES
         */

        List<String> tawatawaUses = new ArrayList<>();
        tawatawaUses.add("high blood pressue");
        tawatawaUses.add("diuretic");
        tawatawaUses.add("anti bacterial");
        tawatawaUses.add("anti fungal");
        tawatawaUses.add("dengue");
        tawatawaUses.add("asthma");
        tawatawaUses.add("bronchitis");
        tawatawaUses.add("ulcers");

        Uses tawaTawaUsesObj = new Uses(14, tawaTawa.getHerbalName(), tawatawaUses);

        /**
         * TAWA TAWA PREPARATIONS
         */
        List<String> tawatawaPreparationList = new ArrayList<>();
        tawatawaPreparationList.add("Take 5 whole TawaTawa plants. Cut off the roots, then wash and clean. Boil TawaTawa in a pot of clean water. Pour the liquid and then let cool. Sip 1 glass 3 to 4 times a day");

        HerbalUsesPreparation tawatawaPreparation = new HerbalUsesPreparation("Tawa-tawa tea tastes bad, it's like drinking soil",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/tawa_tawa", 14, tangladPreparationList);


        /**
         * BAYABAS USES
         */
        List<String> bayabasUses = new ArrayList<>();
        bayabasUses.add("diarrhea");
        bayabasUses.add("obesity");
        bayabasUses.add("diabetes");
        bayabasUses.add("cholesterol");
        bayabasUses.add("gastic cancer");
        bayabasUses.add("prostate cancer");

        Uses bayabasUsesObj = new Uses(15, bayabas.getHerbalName(), bayabasUses);

        /**
         * BAYABAS PREPARATIONS
         */
        List<String> bayabasPreparationList = new ArrayList<>();
        bayabasPreparationList.add("Guava Leaf Tea: " + "Add the leaves in the water and cook them for as long as 15 minutes. Filter the liquid to remove any solid pieces. Drink the tea (as if we had to tell you)!.");
        bayabasPreparationList.add("Crushed Guava Leaf Recipe: " + "The kind of guava leaves you need are of the tender variety, which you should first dry and then crush into powdered form. Once you’re done doing that, add 1 tablespoon of this powder to a cup (glass/mug) of hot water." +
                "Let it sit for about 5 minutes, keeping it covered with any type of lid. Now just strain and drink this tea once a day.");

        HerbalUsesPreparation bayabaPreparation = new HerbalUsesPreparation("really good taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/guava", 15, bayabasPreparationList);

        /**
         * AMPALAYA USES
         */

        List<String> ampalayaUses = new ArrayList<>();
        ampalayaUses.add("cough");
        ampalayaUses.add("fever");
        ampalayaUses.add("worms");
        ampalayaUses.add("headache");
        ampalayaUses.add("diabetes");
        ampalayaUses.add("wounds");
        ampalayaUses.add("hemorroids");
        ampalayaUses.add("diarrhea");

        Uses ampalayaUsesObj = new Uses(16, ampalaya.getHerbalName(), ampalayaUses);

        /**
         * AMPALAYA PREPARATION
         */

        List<String> ampalayaPreparationList = new ArrayList<>();
        ampalayaPreparationList.add("For coughs, fever, worms, diarrhea, diabetes, juice Ampalaya leaves and drink a spoonful daily.");
        ampalayaPreparationList.add("For other ailments, the fruit and leaves can both be juiced and taken orally.");
        ampalayaPreparationList.add("For headaches wounds, burns and skin diseases, apply warmed leaves to afflicted area.");
        ampalayaPreparationList.add("Powdered leaves, and the root decoction, may be used as stringent and applied to treat hemorrhoids.");
        ampalayaPreparationList.add("Internal parasites are proven to be expelled when the ampalaya juice, made from its leaves, is extracted. The ampalaya juice, and grounded seeds is to be taken one spoonful thrice a day, which also treats diarrhea, dysentery, and chronic colitis.");

        HerbalUsesPreparation ampalayaPreparation = new HerbalUsesPreparation("Bitter", "android.resource://mariz.usm.app.com.herballeafidentifier/raw/ampalaya", 16, ampalayaPreparationList);


        /**
         * GOTU KOLA USES
         */
        List<String> gotukolaUses = new ArrayList<>();
        gotukolaUses.add("fatigue");
        gotukolaUses.add("anxiety");
        gotukolaUses.add("colds");
        gotukolaUses.add("flu");
        gotukolaUses.add("sunstroke");
        gotukolaUses.add("tonsilitis");
        gotukolaUses.add("hepatitis");
        gotukolaUses.add("uti");
        gotukolaUses.add("diarrhea");
        gotukolaUses.add("indigestion");

        Uses gotukolaUsesObj = new Uses(17, gotuKola.getHerbalName(), gotukolaUses);

        /**
         * GOTUKOLA PREPARATION
         */
        List<String> gotukolaPreparationList = new ArrayList<>();
        gotukolaPreparationList.add("Dried gotu kola leaves are typically steeped and prepared as tea. To make a cup of gotu kola tea, you need to add 1-2 teaspoons (5 to 10 grams) into 2/3 cup of boiling water and allow the mixture to steep for 10 to 15 minutes.  " +
                "Suggested intake is 1 to 3 cups (750 mL) per day");
        gotukolaPreparationList.add("Tinctures come in vastly different strengths. The extract ratio will tell you how potent the tincture is. For example, 1:1 tincture has 1 ounce of dried herb in 1 ounce of solvent. On the other hand, tincture with a ratio of 1:4 contains 1 ounce of dried herb in 4 ounces of solvent, " +
                "so it is much less potent than the first tincture.");
        gotukolaPreparationList.add("Gotu kola capsules contain dried powder made from gotu kola leaves (sometimes also stem). Typical amount of gout kola in 1 capsule is between 300 and 500 mg and the general recommended use is 2 capsules per day");

        HerbalUsesPreparation gotukolaPreparation = new HerbalUsesPreparation("Gotu Kola is bitter and astringent in taste and cooling in action.",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/gotu_kola", 17, gotukolaPreparationList);


        /**
         * HERBA BUENA USES
         */

        List<String> herbaBuenaUses = new ArrayList<>();
        herbaBuenaUses.add("diarrhea");
        herbaBuenaUses.add("stomachache");
        herbaBuenaUses.add("bad breath");
        herbaBuenaUses.add("insect bites");
        herbaBuenaUses.add("nausea");
        herbaBuenaUses.add("asthma");
        herbaBuenaUses.add("menstrual cramps");

        Uses herbabuenaUsesObj = new Uses(18, herbaBuena.getHerbalName(), herbaBuenaUses);

        /**
         * HERBA BUENA PREPARATIONS
         */

        List<String> herbabuenaPreparations = new ArrayList<>();
        herbabuenaPreparations.add("Herba Buena Tea: " + "Dry Yerba buena leaves for about two weeks. " + "Crush Yerba buena leaves into pieces. " +
                "Boil the dried Yerba buena leaves in water. " + "1 tablespoon of dried Yerba buena leaves to one cup water. " + "Let it seep for 30 minutes. " + "Strain the leaves. " +
                "Take the Yerba buena herbal tea for 4 to 6 times daily.");

        herbabuenaPreparations.add("Herba Buena Inhaler: " + "Put 4 to 5 drops of yerba Buena oil or 10 fresh leaves to a bowl of water. " + "Let it boil. " + "Inhale the vapor");

        HerbalUsesPreparation herbaBuenaPreparation = new HerbalUsesPreparation("minty flavor",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/herba_buena", 18, herbabuenaPreparations);


        /**
         * OKRA USES
         */

        List<String> okraUses = new ArrayList<>();
        okraUses.add("mucous membrane");
        okraUses.add("cough");
        okraUses.add("wounds");
        okraUses.add("fever");
        okraUses.add("muscles spasms");
        okraUses.add("pimples");
        okraUses.add("acne");
        okraUses.add("diabetes");

        Uses okraUsesObj = new Uses(19, okra.getHerbalName(), okraUses);

        /**
         * OKRA PREPARATIONS
         */
        List<String> okraPreparationList = new ArrayList<>();
        okraPreparationList.add("Okra is available in most grocery and vegetable stores. Buy okra fruits that are fresh, young, tender and firm. You will know it is fresh if it can easily be snapped in two. The best okra variety is the green color.");
        okraPreparationList.add("If you have to store okra for later preparation, do not wash as it will become slimy, put in a paper bag and refrigerate – do not freeze. Okra is best consumed within 3 days.");
        okraPreparationList.add("To maintain the best nutritional value of okra, it is suggested not to overcook it.");

        HerbalUsesPreparation okraPreparation = new HerbalUsesPreparation("mildtaste similar to eggplant",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/okra", 19, okraPreparationList);


        /**
         * SILING LABUYO USES
         */

        List<String> silingLabuyoUses = new ArrayList<>();
        silingLabuyoUses.add("bleeding");
        silingLabuyoUses.add("gout");
        silingLabuyoUses.add("rheumatism");
        silingLabuyoUses.add("sores");
        silingLabuyoUses.add("colds");
        silingLabuyoUses.add("fever");
        silingLabuyoUses.add("sore throat");
        silingLabuyoUses.add("toothache");
        silingLabuyoUses.add("diabetes");
        silingLabuyoUses.add("typhus");

        Uses silingLabuyoUsesObj = new Uses(20, siliLabuyo.getHerbalName(), silingLabuyoUses);

        /**
         * SILING LABUYO PREPARATIONS
         */

        List<String> silingLabuyoPreparationList = new ArrayList<>();
        silingLabuyoPreparationList.add("A decoction of half a teaspoon of siling labuyo mixed with warm water taken twice a day can alleviate symptoms of heavy bleeding.");
        silingLabuyoPreparationList.add("Crushed siling labuyo is mixed with oil and massaged to joint pains, gout, arthritis and rheumatism to relieve pain and inflammation");
        silingLabuyoPreparationList.add("Crushed siling labuyo is a strong rubefacient, or irritant that makes the skin to become red by causing dilation of the capillaries and an increase in blood circulation.");
        silingLabuyoPreparationList.add("Crushed leaves of siling labuyo are applied to skin wounds and sores, believed to possess antibiotic properties.");
        silingLabuyoPreparationList.add("Siling labuyo infusion is used in treating cough and stuffed nose due to colds and fever. It is used to stimulate mucus flow from sinus cavities clearing the congestion.");
        silingLabuyoPreparationList.add("Siling labuyo is used to treat sore throat when gargled.");
        silingLabuyoPreparationList.add("Infusion of siling labuyo is used as body stimulant and antispasmodic. It is used to treat flatulence and dyspepsia");
        silingLabuyoPreparationList.add("Juice from crushed siling labuyo is applied into tooth cavities to relieve toothache.");
        silingLabuyoPreparationList.add("Siling labuyo is believed to lower blood cholesterol and can help cure diabetes.");
        silingLabuyoPreparationList.add("Infusion of siling labuyo is used to treat typhus and fevers.");

        HerbalUsesPreparation silingLabuyoPreparation = new HerbalUsesPreparation("pungent odor and taste",
                "android.resource://mariz.usm.app.com.herballeafidentifier/raw/siling_labuyo", 20, silingLabuyoPreparationList);

        List<Herbals> list = new ArrayList<>();
        list.add(oregano);
        list.add(alugbati);
        list.add(mayana);
        list.add(lagundi);
        list.add(pansitpansitan);
        list.add(gingerLeaves);
        list.add(tsaangGubat);
        list.add(akapulko);
        list.add(guyabano);
        list.add(sambong);
        list.add(tanglad);
        list.add(malunggay);
        list.add(saluyot);
        list.add(tawaTawa);
        list.add(bayabas);
        list.add(ampalaya);
        list.add(gotuKola);
        list.add(herbaBuena);
        list.add(okra);
        list.add(siliLabuyo);

        List<Uses> herbalUses = new ArrayList<>();
        herbalUses.add(oreganoUsesObj);
        herbalUses.add(alugbatiUsesObj);
        herbalUses.add(mayanaUsesObj);
        herbalUses.add(lagundiUsesObj);
        herbalUses.add(pansitanUsesObj);
        herbalUses.add(gingerUsesObj);
        herbalUses.add(tsaangGubatUsesObj);
        herbalUses.add(akapulkoUsesObj);
        herbalUses.add(guyabanoUsesObj);
        herbalUses.add(sambongUsesObj);
        herbalUses.add(tangladUsesObj);
        herbalUses.add(malunggayUsesObj);
        herbalUses.add(saluyotUsesObj);
        herbalUses.add(tawaTawaUsesObj);
        herbalUses.add(bayabasUsesObj);
        herbalUses.add(ampalayaUsesObj);
        herbalUses.add(gotukolaUsesObj);
        herbalUses.add(herbabuenaUsesObj);
        herbalUses.add(okraUsesObj);
        herbalUses.add(silingLabuyoUsesObj);


        List<HerbalUsesPreparation> preparations = new ArrayList<>();
        preparations.add(oreganoPreparation);
        preparations.add(alugbatiPreparation);
        preparations.add(mayanaPreparation);
        preparations.add(lagundiPreparation);
        preparations.add(pansitanPreparation);
        preparations.add(gingerPreparation);
        preparations.add(tsaangGubatPreparation);
        preparations.add(akapulkoPreparation);
        preparations.add(guyabanoPreparation);
        preparations.add(sambongPreparation);
        preparations.add(tangladPreparation);
        preparations.add(malunggayPreparation);
        preparations.add(saluyotPreparation);
        preparations.add(tawatawaPreparation);
        preparations.add(bayabaPreparation);
        preparations.add(ampalayaPreparation);
        preparations.add(gotukolaPreparation);
        preparations.add(herbaBuenaPreparation);
        preparations.add(okraPreparation);
        preparations.add(silingLabuyoPreparation);


        if (HerbalDatabase.getInstance(ctx).getHerbalDao().checkIfCurrentHerbalAlreadyExist().size() != 0) {
            //files exist in database
            Log.e(TAG, "initDbContent: Currently Exists in Database!");
        } else {

            for (Herbals h : list) {
                HerbalDatabase.getInstance(ctx).getHerbalDao().insertHerbal(h);
            }

            for (Uses u : herbalUses) {
                for (String use : u.getIlls()) {
                    Uses uses = new Uses(use, u.getHerbal_id(), u.getHerbalName());
                    HerbalDatabase.getInstance(ctx).getUsesDao().insertHerbalUses(uses);
                }
            }

            for (HerbalUsesPreparation prep : preparations) {
                for (String p : prep.getDescription()) {
                    HerbalUsesPreparation usesPreparation = new HerbalUsesPreparation(p, prep.getTaste(), prep.getVideoUri(), prep.getHerbal_id());
                    HerbalDatabase.getInstance(ctx).getHerbalUsesPreparationDao().insertHerbalPreparation(usesPreparation);
                }
            }


        }

    }
}
