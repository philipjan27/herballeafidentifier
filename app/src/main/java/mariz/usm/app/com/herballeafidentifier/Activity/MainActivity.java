package mariz.usm.app.com.herballeafidentifier.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.ImageReader;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import mariz.usm.app.com.herballeafidentifier.Helpers.HerbalDataContents;
import mariz.usm.app.com.herballeafidentifier.R;

public class MainActivity extends AppCompatActivity implements ImageReader.OnImageAvailableListener, View.OnClickListener {

    TextView capture, search, about;
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        capture = findViewById(R.id.tv_captureimage);
        search = findViewById(R.id.tv_searchherbs);
        about = findViewById(R.id.tv_aboutapp);

        capture.setOnClickListener(this);
        search.setOnClickListener(this);
        about.setOnClickListener(this);

        Typeface typeface= ResourcesCompat.getFont(this,R.font.caviar_dreams);
        capture.setTypeface(typeface);
        search.setTypeface(typeface);
        about.setTypeface(typeface);

        HerbalDataContents.getInstance(this).initDbContent();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onImageAvailable(ImageReader imageReader) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_captureimage:
                startActivity(new Intent(this, RecognitionActivity.class));
                break;
            case R.id.tv_searchherbs:
                startActivity(new Intent(this, HerbalSearchActivity.class));
                break;
            case R.id.tv_aboutapp:
                showAboutAppDialog();
                break;
        }
    }

    private void showAboutAppDialog() {
        MaterialDialog.Builder builder = new  MaterialDialog.Builder(this)
                .backgroundColor(this.getResources().getColor(R.color.colorAccent))
                .title(R.string.about_title)
                .titleColor(this.getResources().getColor(R.color.text_white))
                .contentColor(this.getResources().getColor(R.color.text_white))
                .content(R.string.about_shortdesc)
                .positiveText("Dismiss")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                });

        MaterialDialog d= builder.build();
        d.show();

    }

}
