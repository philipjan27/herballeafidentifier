package mariz.usm.app.com.herballeafidentifier.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.Models.Uses;

/**
 * Created by Philip on 12/20/2017.
 */

@Dao
public interface UsesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertHerbalUses(Uses...uses);

    @Query("SELECT * FROM Uses")
    List<Uses> getListOfHerbalUses();

    @Query("SELECT * FROM Uses WHERE herbal_id = :herbal_id")
    List<Uses> getAllUsesInSingleHerbalPlants(int herbal_id);

    @Query("SELECT * FROM herbals INNER JOIN uses ON herbals.id=uses.herbal_id  WHERE uses.id=:usesId")
    List<Herbals> herbalList(int usesId);
}
