package mariz.usm.app.com.herballeafidentifier.Views;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by Philip on 12/09/2017.
 */

public class CustomCameraPreviewer extends SurfaceView implements SurfaceHolder.Callback {


    private Camera.Parameters params;
    private Camera camInstance;
    private SurfaceHolder sHolder;
    private static final String TAG=CustomCameraPreviewer.class.getSimpleName();

    public CustomCameraPreviewer(Context context,Camera camInstance) {
        super(context);
        this.camInstance=camInstance;
        sHolder=getHolder();
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        sHolder.addCallback(this);

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        if (surfaceHolder != null) {

            try {
                params= camInstance.getParameters();
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                camInstance.setParameters(params);

                camInstance.setPreviewDisplay(surfaceHolder);
                camInstance.startPreview();
            } catch (IOException e) {
                Log.e(TAG, "surfaceCreated: "+e.getMessage() );
                Toast.makeText(getContext(),"Exception: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camInstance.stopPreview();
        camInstance.release();
    }
}
