package mariz.usm.app.com.herballeafidentifier.Views;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Database.HerbalDatabase;
import mariz.usm.app.com.herballeafidentifier.Models.HerbalUsesPreparation;
import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.Models.Uses;
import mariz.usm.app.com.herballeafidentifier.R;


public class HerbalContentFragment extends Fragment {

    public final static String TAG = HerbalContentFragment.class.getSimpleName();

    Herbals herbal;
    List<Uses> usesList= new ArrayList<>();
    List<HerbalUsesPreparation> preparationList;
    List<String> list= new ArrayList<>();

    int herbalId;
    String herbal_name;
    String uses;

    View v;
    ImageView imageView;
    TextView herbalName, herbalScientificName,botanyContent, usesContent,herbalTaste;
    VideoView videoView;
    MediaController mediaController;
    LinearLayout preparationsContent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        herbalId = getArguments().getInt("key");
        herbal_name = getArguments().getString("key");
        Log.e(TAG, "onCreate: KEY: " + herbalId + herbal_name);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_herbal_content, container, false);
        imageView = (ImageView) v.findViewById(R.id.herbal_imageview);
        herbalName = v.findViewById(R.id.herbal_name);
        herbalScientificName = v.findViewById(R.id.herbal_scientificname);
        botanyContent= v.findViewById(R.id.herbal_botanycontent);
        usesContent= v.findViewById(R.id.herbal_usesforcontent);
        preparationsContent= v.findViewById(R.id.preparations_container);
        herbalTaste= v.findViewById(R.id.herbal_taste);
        videoView= v.findViewById(R.id.herbal_video);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (herbalId != 0) {
            herbal = HerbalDatabase.getInstance(getActivity()).getHerbalDao().getSingleHerbalContent(herbalId);
            Log.e(TAG, "onResume: " + herbal.getHerbalName());
            usesList= HerbalDatabase.getInstance(getActivity()).getUsesDao().getAllUsesInSingleHerbalPlants(herbal.getId());
            Log.e(TAG, "onResume: List Size: "+usesList.size() );
            preparationList= HerbalDatabase.getInstance(getActivity()).getHerbalUsesPreparationDao().getAllHerbalUsesPreparationInSingleHerbal(herbal.getId());
            Log.e(TAG, "onResume: Preparations Size: "+preparationList.size());
        } else {
            herbal = HerbalDatabase.getInstance(getActivity()).getHerbalDao().getHerbalName(herbal_name);
            Log.e(TAG, "onResume: " + herbal.getHerbalName());
            usesList= HerbalDatabase.getInstance(getActivity()).getUsesDao().getAllUsesInSingleHerbalPlants(herbal.getId());
            Log.e(TAG, "onResume: List Size: "+usesList.size() );
            preparationList= HerbalDatabase.getInstance(getActivity()).getHerbalUsesPreparationDao().getAllHerbalUsesPreparationInSingleHerbal(herbal.getId());
            Log.e(TAG, "onResume: Preparations Size: "+preparationList.size());
        }

        getListOfUses();
        getListOfPreparations();
        setVideoViewContents();

        getHerbalImageToDisplay(herbal.getHerbalName(), imageView);
        herbalName.setText(herbal.getHerbalName());
        herbalScientificName.setText(herbal.getScientificName());
        botanyContent.setText(herbal.getBotanyDescription());
        usesContent.setText(uses);
        herbalTaste.setText(preparationList.get(0).getTaste());

        makeSnackBar();
    }

    private void getListOfUses(){
        StringBuilder sb= new StringBuilder();
        for (Uses use : usesList) {
            sb.append(use.getIllness()+" ");
        }
        uses= sb.toString();
    }

    private void getListOfPreparations(){
        for (HerbalUsesPreparation prep: preparationList){
            inflatePreparations(prep.getShortDesc());
        }
    }

    private void inflatePreparations(String description){
        TextView tv_description;
        View preparationView= getLayoutInflater().inflate(R.layout.preparation_customview,null);

        tv_description= preparationView.findViewById(R.id.preparation_description);
        tv_description.setText(description);

        preparationsContent.addView(preparationView);

    }

    private void setVideoViewContents(){

        mediaController= new MediaController(getActivity());
        videoView.setVideoURI(Uri.parse(preparationList.get(0).getVideoUri()));
        videoView.setMediaController(mediaController);
        videoView.requestFocus();
//        videoView.bringToFront();
        mediaController.setAnchorView(videoView);
        //videoView.setZOrderOnTop(true);  //let video view overlay parentview to top
        videoView.seekTo(100);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: VIDEO");
                videoView.start();
            }
        });
//       videoView.start();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause: ");
        super.onPause();
    }

    public void getHerbalImageToDisplay(String herbalName, ImageView imgvw) {
        Log.e(TAG, "getHerbalImageToDisplay: " + herbalName);
        switch (herbalName) {
            case "akapulko":
                imgvw.setImageResource(R.drawable.akapulko);
                break;
            case "alugbati":
                imgvw.setImageResource(R.drawable.alugbati);
                break;
            case "ampalaya":
                imgvw.setImageResource(R.drawable.ampalaya);
                break;
            case "bayabas":
                imgvw.setImageResource(R.drawable.bayabas);
                break;
            case "gingerleaves":
                imgvw.setImageResource(R.drawable.ginger_leaves);
                break;
            case "gotukola":
                imgvw.setImageResource(R.drawable.gotu_kola);
                break;
            case "guyabano":
                imgvw.setImageResource(R.drawable.guyabano);
                break;
            case "herbabuena":
                imgvw.setImageResource(R.drawable.herba_buena);
                break;
            case "lagundi":
                imgvw.setImageResource(R.drawable.lagundi);
                break;
            case "malunggay":
                imgvw.setImageResource(R.drawable.malunggay);
                break;
            case "mayana":
                imgvw.setImageResource(R.drawable.mayana);
                break;
            case "okra":
                imgvw.setImageResource(R.drawable.okra);
                break;
            case "oregano":
                imgvw.setImageResource(R.drawable.oregano);
                break;
            case "pansitpansitan":
                imgvw.setImageResource(R.drawable.pansit_pansitan);
                break;
            case "saluyot":
                imgvw.setImageResource(R.drawable.saluyot);
                break;
            case "sambong":
                imgvw.setImageResource(R.drawable.sambong);
                break;
            case "sililabuyo":
                imgvw.setImageResource(R.drawable.sili);
                break;
            case "tanglad":
                imgvw.setImageResource(R.drawable.tanglad);
                break;
            case "tawatawa":
                imgvw.setImageResource(R.drawable.tawa_tawa);
                break;
            case "tsaanggubat":
                imgvw.setImageResource(R.drawable.tsaang_gubat);
                break;
        }
    }

    private void makeSnackBar() {
        final Snackbar snackbar= Snackbar.make(getView(),"Disclaimer: If symptoms persist consult your doctor.",5000);
        snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

}
