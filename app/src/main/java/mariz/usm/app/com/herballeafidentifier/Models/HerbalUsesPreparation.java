package mariz.usm.app.com.herballeafidentifier.Models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by Philip on 12/19/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = Herbals.class, parentColumns = "id", childColumns = "herbal_id", onUpdate = 5, onDelete = 5))
public class HerbalUsesPreparation {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String shortDesc;
    private String taste;
    private String videoUri;
    private int herbal_id;

    @Ignore
    private List<String> description;

    @Ignore
    public HerbalUsesPreparation(int id, String shortDesc, String taste, String videoUri, int herbal_id, List<String> description) {
        this.id = id;
        this.shortDesc = shortDesc;
        this.taste = taste;
        this.videoUri = videoUri;
        this.herbal_id = herbal_id;
        this.description = description;
    }

    @Ignore
    public HerbalUsesPreparation( String taste, String videoUri, int herbal_id, List<String> description) {
        this.taste = taste;
        this.videoUri = videoUri;
        this.herbal_id = herbal_id;
        this.description = description;
    }

    public HerbalUsesPreparation(String shortDesc, String taste, String videoUri, int herbal_id) {
        this.shortDesc = shortDesc;
        this.taste = taste;
        this.videoUri = videoUri;
        this.herbal_id = herbal_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public int getHerbal_id() {
        return herbal_id;
    }

    public void setHerbal_id(int herbal_id) {
        this.herbal_id = herbal_id;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }
}
