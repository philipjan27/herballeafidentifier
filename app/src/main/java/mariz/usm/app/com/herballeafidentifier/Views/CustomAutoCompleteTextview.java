package mariz.usm.app.com.herballeafidentifier.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import java.util.HashMap;
import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Models.Uses;

/**
 * Created by Philip on 12/26/2017.
 */

public class CustomAutoCompleteTextview extends android.support.v7.widget.AppCompatAutoCompleteTextView {

    public final static String TAG = CustomAutoCompleteTextview.class.getSimpleName();

    public CustomAutoCompleteTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {

        HashMap<String,String> stringStringHashMap= (HashMap<String,String>) selectedItem;

        Log.e(TAG, "convertSelectionToString: "+selectedItem);
        Log.e(TAG, "convertSelectionToString: "+stringStringHashMap.get("illness"));
        return stringStringHashMap.get("illness");

    }
}
