package mariz.usm.app.com.herballeafidentifier.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import mariz.usm.app.com.herballeafidentifier.Models.HerbalUsesPreparation;

/**
 * Created by Philip on 12/20/2017.
 */

@Dao
public interface HerbalUsesPreparationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertHerbalPreparation(HerbalUsesPreparation...hup);

    @Query("SELECT * FROM HerbalUsesPreparation")
    List<HerbalUsesPreparation> getAllHerbalUsesAndPreparation();

    @Query("SELECT * FROM HerbalUsesPreparation WHERE herbal_id = :herbal_id")
    List<HerbalUsesPreparation> getAllHerbalUsesPreparationInSingleHerbal(int herbal_id);
}
