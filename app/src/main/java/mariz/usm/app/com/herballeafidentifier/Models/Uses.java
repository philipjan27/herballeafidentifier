package mariz.usm.app.com.herballeafidentifier.Models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by Philip on 12/19/2017.
 */

@Entity(foreignKeys = @ForeignKey(entity = Herbals.class,parentColumns = "id",childColumns = "herbal_id",onDelete = 5,onUpdate = 5))
public class Uses {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String illness;
    private int herbal_id;
    private String herbalName;

    @Ignore
    private List<String> ills;

    @Ignore
    public Uses(int id, String illness, int herbal_id) {
        this.id = id;
        this.illness = illness;
        this.herbal_id = herbal_id;
    }

    @Ignore
    public Uses(String illness, List<String> ills, int herbal_id, String herbalName) {
        this.illness = illness;
        this.ills=ills;
        this.herbal_id = herbal_id;
        this.herbalName=herbalName;
    }

    @Ignore
    public Uses(int herbal_id, String herbalName, List<String> ills) {
        this.herbal_id = herbal_id;
        this.herbalName = herbalName;
        this.ills = ills;
    }

    public Uses(String illness, int herbal_id, String herbalName) {
        this.illness = illness;
        this.herbal_id = herbal_id;
        this.herbalName = herbalName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIllness() {
        return illness;
    }

    public void setIllness(String illness) {
        this.illness = illness;
    }

    public int getHerbal_id() {
        return herbal_id;
    }

    public void setHerbal_id(int herbal_id) {
        this.herbal_id = herbal_id;
    }

    public String getHerbalName() {
        return herbalName;
    }

    public void setHerbalName(String herbalName) {
        this.herbalName = herbalName;
    }

    public void setIlls(List<String> ills) {
        this.ills = ills;
    }

    public List<String> getIlls() {
        return ills;
    }
}
