package mariz.usm.app.com.herballeafidentifier.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import mariz.usm.app.com.herballeafidentifier.Models.HerbalUsesPreparation;
import mariz.usm.app.com.herballeafidentifier.Models.Herbals;
import mariz.usm.app.com.herballeafidentifier.Models.Uses;

/**
 * Created by Philip on 12/18/2017.
 */

@Database(entities = {Herbals.class, HerbalUsesPreparation.class,Uses.class}, version = 1)
public abstract class HerbalDatabase extends RoomDatabase {

    private static final String DB_NAME="HerbalPlants.db";
    private static volatile HerbalDatabase instance;

    public static synchronized HerbalDatabase getInstance(Context ctx) {
        if (instance == null) {
            instance= create(ctx);
        }

        return instance;
    }


    static HerbalDatabase create(Context ctx) {
        return Room.databaseBuilder(ctx,HerbalDatabase.class,DB_NAME).allowMainThreadQueries().build();
    }

    public static void destroyDbInstance() {
        if (instance != null) {
            instance=null;
            instance.close();
        }
    }

    public abstract HerbalDao getHerbalDao();
    public abstract HerbalUsesPreparationDao getHerbalUsesPreparationDao();
    public abstract UsesDao getUsesDao();

}
